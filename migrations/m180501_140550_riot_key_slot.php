<?php

use yii\db\Migration;

/**
 * Class m180501_140550_riot_key_slot
 */
class m180501_140550_riot_key_slot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('secret', array(
            'owner' => 'AnthonyWhitebeard',
            'key' => 123,
        ));
        $this->insert('secret', array(
            'owner' => 'fantom79022',
            'key' => 456,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('secret',['owner' => 'fantom79022']);
        $this->delete('secret',['owner' => 'anthonywhitebeard']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180501_140550_riot_key_slot cannot be reverted.\n";

        return false;
    }
    */
}
