<?php

use yii\db\Migration;

/**
 * Class m180618_162105_old_messages
 */
class m180618_162106_fantom extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('fantom', [
            'id' => $this->primaryKey(),
            'count' =>$this->integer(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('fantom');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180618_162105_old_messages cannot be reverted.\n";

        return false;
    }
    */
}
