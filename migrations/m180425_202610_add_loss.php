<?php

use yii\db\Migration;

/**
 * Class m180425_202610_add_loss
 */
class m180425_202610_add_loss extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('phrases', [
            'id' => $this->primaryKey(),
            'key' => $this->text(),
            'answer' => $this->text(),
        ]);
        $this->createTable('losses', [
            'id' => $this->primaryKey(),
            'link' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('phrases');
        $this->dropTable('losses');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180425_202610_add_loss cannot be reverted.\n";

        return false;
    }
    */
}
