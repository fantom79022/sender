<?php

use yii\db\Migration;

/**
 * Class m180611_172201_royal
 */
class m180611_172201_royal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('royal_battle', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'hp' => $this->integer(),
            'dmg' => $this->integer(),
        ]);
        $this->createTable('royal_stats', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->text(),
            'wins' => $this->integer(),
        ]);
        $this->createTable('royal_reg', [
            'id' =>$this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->text(),
        ]);
        $this->createTable('battle_status', [
            'id' =>$this->primaryKey(),
            'status' =>$this->text(),
        ]);
        $this->insert('battle_status', array(
            'id' => '1',
            'status' => 'off',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('royal_battle');
        $this->dropTable('royal_stats');
        $this->dropTable('royal_reg');
        $this->dropTable('battle_status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180611_172201_royal cannot be reverted.\n";

        return false;
    }
    */
}
