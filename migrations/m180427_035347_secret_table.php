<?php

use yii\db\Migration;

/**
 * Class m180427_035347_secret_table
 */
class m180427_035347_secret_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('secret', [
            'id' => $this->primaryKey(),
            'key' => $this->text(),
            'owner' => $this->text(),
        ]);
        $this->createTable('status', [
            'id' => $this->primaryKey(),
            'status' => $this->text(),
            'owner' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('secret');
        $this->dropTable('status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180427_035347_secret_table cannot be reverted.\n";

        return false;
    }
    */
}
