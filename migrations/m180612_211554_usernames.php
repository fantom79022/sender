<?php

use yii\db\Migration;

/**
 * Class m180612_211554_usernames
 */
class m180612_211554_usernames extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('royal_reg', 'username', $this->text());
        $this->addColumn('royal_battle', 'username', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('royal_battle', 'username');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180612_211554_usernames cannot be reverted.\n";

        return false;
    }
    */
}
