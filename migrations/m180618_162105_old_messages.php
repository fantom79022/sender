<?php

use yii\db\Migration;

/**
 * Class m180618_162105_old_messages
 */
class m180618_162105_old_messages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('old_messages', [
            'id' => $this->primaryKey(),
            'battle_id' =>$this->text(),
            'message_id' =>$this->text(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('old_messages');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180618_162105_old_messages cannot be reverted.\n";

        return false;
    }
    */
}
