<?php

use yii\db\Migration;

/**
 * Class m180227_004349_bigint
 */
class m180227_004349_bigint extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('links', 'tg_chat_id', $this->bigInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('links', 'tg_chat_id', $this->integer());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180227_004349_bigint cannot be reverted.\n";

        return false;
    }
    */
}
