<?php

use yii\db\Migration;

/**
 * Class m180419_150035_lolbase
 */
class m180419_150035_lolbase extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lolbase', [
            'own_id' => $this->primaryKey(),
            'id' => $this->integer(),
            'vk' => $this->text(),
            'university' => $this->text(),
            'summoner_name' => $this->text(),
            'country' => $this->text(),
            'city' => $this->text(),
            'name' => $this->text(),
            's_name' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lolbase');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180419_150035_lolbase cannot be reverted.\n";

        return false;
    }
    */
}
