<?php

use yii\db\Migration;

/**
 * Class m180501_191913_cons_to_base
 */
class m180501_191913_cons_to_base extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('secret', array(
            'owner' => 'BOT_API_KEY',
            'key' => '528796117:AAH4EF_WPr47NTZfj9JaApxe2Lsw1uZRhPA',
            )
        );
        $this->insert('secret', array(
            'owner' => 'ACCESS_TOKEN',
            'key' => '227fa7d660f8b0daf02a197fd7bc4863e9e333f095ca2de4de9dbb721b464eb94236e87562111355a1b3d',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('secret',['owner' => 'BOT_API_KEY']);
        $this->delete('secret',['owner' => 'ACCESS_TOKEN']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180501_191913_cons_to_base cannot be reverted.\n";

        return false;
    }
    */
}
