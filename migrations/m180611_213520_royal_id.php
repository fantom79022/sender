<?php

use yii\db\Migration;

/**
 * Class m180611_213520_royal_id
 */
class m180611_213520_royal_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('royal_battle', 'user_id', $this->integer());
        $this->addColumn('royal_battle', 'mark', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('royal_battle', 'user_id');
        $this->dropColumn('royal_battle', 'mark');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180611_213520_royal_id cannot be reverted.\n";

        return false;
    }
    */
}
