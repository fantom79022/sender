<?php

use yii\db\Migration;

/**
 * Class m180613_181655_lootboxes
 */
class m180613_181655_lootboxes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('awards', [
            'id' => $this->primaryKey(),
            'code' =>$this->text(),
            'more' =>$this->text(),
        ]);
        $this->createTable('owners', [
            'id' => $this->primaryKey(),
            'owner' =>$this->text(),
            'award_id' =>$this->text(),
            'type' =>$this->text(),
        ]);
       $this->createTable('massage_report', [
            'id' => $this->primaryKey(),
            'chat_id' =>$this->text(),
            'massage_id' =>$this->text(),
            'status' =>$this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('awards');
        $this->dropTable('owners');
        $this->dropTable('massage_report');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180613_181655_lootboxes cannot be reverted.\n";

        return false;
    }
    */
}
