<?php

use yii\db\Migration;

/**
 * Class m180219_161943_linls_table
 */
class m180219_161943_linls_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('links', [
            'id' => $this->primaryKey(),
            'vk_chat_id' => $this->text(),
            'tg_chat_id' => $this->integer(),
            'special_code' => $this->text(),
            'conf' => $this->boolean(),
            'status' => $this->text(),
        ]);
        $this->createTable('last_message', [
            'id' => $this->primaryKey(),
            'last_vk_message' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('links');
        $this->dropTable('last_message');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180219_161943_linls_table cannot be reverted.\n";

        return false;
    }
    */
}
