<?php

namespace app\controllers;

use app\models\LastMessage;
use app\models\Links;
use app\models\Lolbase;
use app\models\Secret;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class FromvkController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionApi(){

    }

    public function actionCheck(){
        $id = LastMessage::find()->where(['id' => 1])->one();
        if ($id == null){
            $id = new LastMessage();
            $id->last_vk_message = 0;
            $id->save();
        }
        $msgId = $id->last_vk_message;
        $url = SiteController::API_ADDRESS.'/messages.get?access_token='.siteController::ACCESS_TOKEN.'&last_message_id='.$msgId.'&count=200&v=4.100';
        $update = file_get_contents($url);
        $update = json_decode($update);
        foreach (array_reverse($update->response) as $content){
            if (isset($content->mid)) {
                $this->parsing($content);
                if ($content->mid > $id->last_vk_message){
                    $id->last_vk_message = $content->mid;
                    $id->save();
                }
            }
        }
        die();
    }
    private function parsing($content){
        if ($content->body == '/generate'){
            if (isset($content->chat_id)){
                $link = Links::find()->where(['vk_chat_id' => $content->chat_id])->one();
                if ($link !== null){
                    $spCode = $link->special_code;
                }
                else{
                    $link = new Links();
                    $link->vk_chat_id = (string)$content->chat_id;
                    $link->conf = '1';
                    $spCode = (string)(rand(1, 666)*rand(1, 666)*rand(1, 666)*rand(1, 666)+rand(0, 100)*111+ rand(1, 777)+time());
                    $link->special_code = $spCode;
                    $link->save();
                }
                file_get_contents(SiteController::API_ADDRESS.'/messages.send?access_token='.siteController::ACCESS_TOKEN.'&peer_id='.($link->vk_chat_id+2000000000).'&chat_id='.$link->vk_chat_id.'&message='.$spCode.'&v=5.73');
            }
            else{
                $link = Links::find()->where(['vk_chat_id' => $content->uid])->one();
                if ($link !== null){
                    $spCode = $link->special_code;
                }
                else{
                    $link = new Links();
                    $link->vk_chat_id = (string)($content->uid);
                    $link->conf = '0';
                    $spCode = (string)(rand(1, 666)*rand(1, 666)*rand(1, 666)*rand(1, 666)+rand(0, 100)*111+ rand(1, 777)+time());
                    $link->special_code = $spCode;
                    $link->save();
                }
                file_get_contents(SiteController::API_ADDRESS.'/messages.send?access_token='.siteController::ACCESS_TOKEN.'&peer_id='.$link->vk_chat_id.'&user_id='.$link->vk_chat_id.'&message='.$spCode.'&v=5.73');
            }
        }
        else{

            $token = $bot_api_key  = SiteController::BOT_API_KEY;
            $bot_username = '@tg_vk_sender_bot';
            mb_internal_encoding("UTF-8");
            try {

                $bot = new \TelegramBot\Api\Client($token);
                $tg = new \TelegramBot\Api\BotApi($token);
                $hook = SiteController::HOOK;

                if (isset($content->chat_id)) {
                $link = Links::find()->where(['vk_chat_id' => $content->chat_id])->all();
                }
                else{
                    $link = Links::find()->where(['vk_chat_id' => $content->uid])->all();
                }
                foreach ($link as $lnk){
                    if (($lnk->status == 'all')OR($lnk->status == 'fromvk')OR($lnk->status == null)){
                        usleep(330000);
                        $bodytag = str_replace('<br>', '
', $content->body);
                        $res = '';
                        $usr = json_decode(file_get_contents(SiteController::API_ADDRESS.'/users.get?access_token='.siteController::ACCESS_TOKEN.'&user_ids='.$content->uid.'&v=4.100'));
                         if ($bodytag !== ''){
                            $tg->sendMessage($lnk->tg_chat_id, '<b>'.$usr->response[0]->first_name.' '.$usr->response[0]->last_name.':</b>', 'html');
                             while (strlen($bodytag) > 1500) {
                                 $lng = strpos($bodytag, ' ', 1450);
                                 $text = substr($bodytag, 0, $lng);
                                 $bodytag = substr($bodytag, $lng);
                                 usleep(330000);
                                 $tg->sendMessage($lnk->tg_chat_id, $text, 'HTML');
                             }
                             if (strlen($bodytag) <= 1500){
                                 usleep(330000);
                                 $tg->sendMessage($lnk->tg_chat_id, $bodytag, 'HTML');
                             }
                        }
                        If(isset($content->attachments)){
                            foreach ($content->attachments as $attch){
                                if (isset($attch->photo)){
                                    $this->photoCheck($attch, $lnk);
                                }
                                if (isset($attch->wall)){
                                    $this->wallCheck($attch, $lnk, $tg, $content->uid);
                                }
                                if (isset($attch->link)){
                                    $this->linkCheck($attch, $lnk, $tg);
                                }
                                if (isset($attch->audio)){
                                    $aud = file_get_contents($attch->audio->url);
                                }
                            }
                        }
                        if (isset($content->fwd_messages)){
                            $res = '';
                            $spc = '';
                            $this->forvMsg($res, $content->fwd_messages, $spc, $lnk, $tg, $content->uid);
                        }

                    }
                }
            }
            catch (\Exception $e) {
            }
        }
    }
    private function forvMsg($resend, $fwd_messages, $spc, $lnk, $tg, $uid){
        $spc = $spc.'    ';
        foreach ($fwd_messages as $repmsg){
            usleep(660000);
            $repmsg->body = str_replace('<br>', '
', $repmsg->body);
            $reusr = json_decode(file_get_contents(SiteController::API_ADDRESS.'/users.get?access_token='.siteController::ACCESS_TOKEN.'&user_ids='.$repmsg->uid.'&v=5.8'));
            $resend = '
'.$spc.'/re: '.$reusr->response[0]->first_name.' '.$reusr->response[0]->last_name.'
'.$spc.$repmsg->body;
            $usr = json_decode(file_get_contents(SiteController::API_ADDRESS . '/users.get?access_token='.siteController::ACCESS_TOKEN.'&user_ids=' . $uid . '&v=5.8'));
            $tg->sendMessage($lnk->tg_chat_id, '
' . $spc . $usr->response[0]->first_name . ' ' . $usr->response[0]->last_name . ':</b>','HTML');
            while (strlen($resend) > 1500) {
                $lng = strpos($resend, ' ', 1450);
                $text = substr($resend, 0, $lng);
                $resend = substr($resend, $lng);
                usleep(330000);
                $tg->sendMessage($lnk->tg_chat_id, $text, 'HTML');
            }
            if (strlen($resend) <= 1500){
                usleep(330000);
                $tg->sendMessage($lnk->tg_chat_id, $resend, 'HTML');
            }
            If(isset($repmsg->attachments)){
                foreach ($repmsg->attachments as $attch){
                    if (isset($attch->photo)){
                        usleep(330000);
                        $this->photoCheck($attch, $lnk);
                    }
                    if (isset($attch->wall)){
                        usleep(330000);
                        $this->wallCheck($attch, $lnk, $tg, $repmsg->uid);
                    }
                }
                if (isset($repmsg->fwd_messages)){
                    usleep(330000);
                    $this->forvMsg($resend, $repmsg->fwd_messages, $spc, $lnk, $tg, $uid);
                }
            }
        }
    }

    private function photoCheck($pst, $lnk){
        if (isset($pst->photo)){
            if (isset($pst->photo->src_xxxbig)){
                $sqr = $pst->photo->src_xxxbig;
            }
            else{
                if(isset($pst->photo->src_xxbig)){
                    $sqr = $pst->photo->src_xxbig;
                }
                else {
                    if (isset($pst->photo->src_xbig)){
                        $sqr = $pst->photo->src_xbig;
                    }
                    else{
                        if (isset($pst->photo->src_big)){
                            $sqr = $pst->photo->src_big;
                        }
                        else{
                            if (isset($pst->photo->src)){
                                $sqr = $pst->photo->src;
                            }
                            else{
                                if (isset($pst->photo->src_small)){
                                    $sqr =  $pst->photo->src_small;
                                }
                            }
                        }
                    }
                }
            }
            file_get_contents('https://api.telegram.org/bot'.Secret::find()->where(['owner' => 'BOT_API_KEY'])->one()->key.'/sendPhoto?chat_id='.$lnk->tg_chat_id.'&photo='.$sqr);
        }
    }

    private function linkCheck($attch, $lnk, $tg){
        $tg->sendMessage($lnk->tg_chat_id, $attch->link->url);
    }

    private function wallCheck($attch, $lnk, $tg, $uid){
        if (isset($attch->wall->copy_owner_id)){
            if ($attch->wall->copy_owner_id < 0){
                usleep(330000);
                $owner = json_decode(file_get_contents(SiteController::API_ADDRESS.'/groups.getById?access_token='.siteController::ACCESS_TOKEN.'&group_ids='.abs($attch->wall->copy_owner_id).'&v=5.8'));
                $owner = $owner->response[0]->name;
            }
            else{
                usleep(330000);
                $owner = json_decode(file_get_contents(SiteController::API_ADDRESS.'/users.get?access_token='.siteController::ACCESS_TOKEN.'&user_ids='.$attch->wall->copy_owner_id.'&v=5.8'));
                $owner = $owner->response[0]->first_name.' '.$owner->response[0]->last_name;
            }
        }
        else{
            usleep(330000);
            $owner = json_decode(file_get_contents(SiteController::API_ADDRESS.'/groups.getById?access_token='.siteController::ACCESS_TOKEN.'&group_ids='.abs($attch->wall->from_id).'&v=5.8'));
            $owner = $owner->response[0]->name;
        }
        if (isset($attch->wall->signer_id)){
            usleep(330000);
            $usr = json_decode(file_get_contents(SiteController::API_ADDRESS.'/users.get?access_token='.siteController::ACCESS_TOKEN.'&user_ids='.$attch->wall->signer_id.'&v=5.8'));
            $usr = ', '.$usr->response[0]->first_name.' '.$usr->response[0]->last_name;
        }
        else {
            $usr = '';
        }
        usleep(330000);
        $url = SiteController::API_ADDRESS.'/users.get?access_token='.siteController::ACCESS_TOKEN.'&user_ids='.$uid.'&v=5.8';
        $sender = json_decode(file_get_contents($url));
        $sender = $sender->response[0]->first_name.' '.$sender->response[0]->last_name.', ';
        $postText = str_replace('<br>', '
', $attch->wall->text);
        $tg->sendMessage($lnk->tg_chat_id, '<i>'.$sender.'</i><b>'.$owner.$usr.':</b>
', 'HTML');
        while (strlen($postText) > 1500) {
            $lng = strpos($postText, ' ', 1450);
            $text = substr($postText, 0, $lng);
            $postText = substr($postText, $lng);
            usleep(330000);
            $tg->sendMessage($lnk->tg_chat_id, $text, 'HTML');
        }
        if (strlen($postText) <= 1500){
            if ($postText != ''){
                usleep(330000);
                $tg->sendMessage($lnk->tg_chat_id, $postText, 'HTML');
            }
        }
        foreach ($attch->wall->attachments as $pst){
            if (isset($pst->photo)){
                $this->photoCheck($pst, $lnk);
            }
            if (isset($pst->wall)){
                $this->wallCheck($pst, $lnk, $tg, $uid);
            }
            if (isset($pst->link)){
                $this->linkCheck($pst, $lnk, $tg);
            }
        }
    }
}
