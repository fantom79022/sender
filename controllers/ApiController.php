<?php

namespace app\controllers;

use app\models\Lolbase;
use app\models\Losses;
use app\models\Phrases;
use app\models\Secret;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class ApiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionFantom()
    {
        $file = 'fantom.txt';
        $input = file_get_contents('php://input');

        if ($input) {
            $data = json_decode($input, true);
            $current = json_encode($data);
            file_put_contents($file, $current);

        }
        $current = file_get_contents('fantom.txt');

//        try {
//            $data = json_decode(file_get_contents('php://input'), true);
//            $id = $data['callback_query']['data'];
//
//            $counter = Fantom::findOne($id);
//            $counter->count = $counter->count + 1;
//            $counter->save();
//        } catch (\Exception $e) {}
        echo($current);
        die;
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionLolBase(){

    }
//    public function actionDownloadsummoner(){
//
//        $x = 1469;
//        $key = SiteController::RIOT_API;
//        while ($x < 1973){
//            $curl_handle=curl_init();
//            curl_setopt($curl_handle, CURLOPT_URL,'https://ulol.ru.leagueoflegends.com/api/users?page='.$x);
//            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
//            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
//            $query = curl_exec($curl_handle);
//            curl_close($curl_handle);
//            $qq = json_decode($query);
//            foreach ($qq->results->entries as $block) {
//                $page = new Lolbase();
//                usleep(500000);
//                $summoner_name = str_replace(' ', '%20', $block->summoner_name);
//                $curl_handle=curl_init();
//                curl_setopt($curl_handle, CURLOPT_URL,SiteController::RIOT_GAMES.'summoner/v3/summoners/by-name/'.$summoner_name.'?'.$key);
//                curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
//                curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
//                curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
//                $query = curl_exec($curl_handle);
//                curl_close($curl_handle);
//                $summoner = json_decode($query);
//                if (isset($summoner->id)){
//                    $page->id = $summoner->id;
//                }
//                if (isset($block->vk->href)) {
//                    $page->vk = $block->vk->href;
//                }
//                $page->university = $block->university;
//                $page->summoner_name = $block->summoner_name;
//                $page->city = $block->city;
//                $page->country = $block->country;
//                if (isset($block->vk->first_name)){
//                    $page->name = $block->vk->first_name;
//                }
//                if (isset($block->vk->last_name)){
//                    $page->s_name = $block->vk->last_name;
//                }
//                $page->save();
//            }
//            $x++;
//            if (($x % 12) == 0){
//                sleep(35);
//                $key = SiteController::RIOT_API;
//
//            } else{
//                if ((($x % 6) == 0)){
//                    sleep(35);
//                    $key = 'Site::RIOT_API2';
//                }
//            }
//        }
//        die();
//    }

//    public function actionFixbase(){
//        $summoner = Lolbase::find()->where(['id' => null])->orderBy('country desc')->all();
//        foreach ($summoner as $unknown){
//            sleep(2);
//            $curl_handle=curl_init();
//            $summoner_name = str_replace(' ', '%20', $unknown->summoner_name);
//            $adr = SiteController::RIOT_GAMES.'summoner/v3/summoners/by-name/'.$summoner_name.'?'.SiteController::RIOT_API;
//            curl_setopt($curl_handle, CURLOPT_URL, $adr);
//            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
//            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
//            $query = curl_exec($curl_handle);
//            curl_close($curl_handle);
//            $sum = json_decode($query);
//            if (isset($sum->id)){
//                $unknown->id = $sum->id;
//                $unknown->update();
//            }
//        }
//    }

    //api for summoner info
    public function actionSummoner($summoner){
        $summoner_name = str_replace(' ', '%20', $summoner);
        $adr = SiteController::RIOT_GAMES.'summoner/v3/summoners/by-name/'.$summoner_name.'?api_key='.Secret::find()->where(['owner'=>'AnthonyWhitebeard'])->one()->key;
        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $adr);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
        $query = curl_exec($curl_handle);
        curl_close($curl_handle);
        $sum = json_decode($query);
        if ((isset($sum->status->status_code)) and ($sum->status->status_code == 404)){
            $player['error'] = 'player not found in RIOT base';
        } else{
            if ($sum !== null){
                if ((isset($sum->status)) and ($sum->status->message == 'Forbidden')){
                    $player['error'] = 'Riot keys error';
                    die(json_encode($player));
                }
                $summoner = Lolbase::find()->where(['id' => $sum->id])->orderBy('country desc')->one();
            } else{
                $summoner = Lolbase::find()->where(['summoner_name' => $summoner])->orderBy('country desc')->one();
            }
            if ($summoner !== null){
                $player['name'] = $summoner->name;
                $player['s_name'] = $summoner->s_name;
                $player['country'] = $summoner->country;
                $player['city'] = $summoner->city;
                $player['university'] = $summoner->university;
                $player['summoner_name'] = $summoner->summoner_name;
                $player['vk'] = $summoner->vk;
            }
            else{
                $player['error'] = 'player not found in a base';
            }
        }
        die(json_encode($player, JSON_UNESCAPED_UNICODE));
    }

    //api for current match info
    public function actionTeamtards($summoner){
        $summoner_name = str_replace(' ', '%20', $summoner);

        $adr = SiteController::RIOT_GAMES.'summoner/v3/summoners/by-name/'.$summoner_name.'?api_key='.Secret::find()->where(['owner'=>'AnthonyWhitebeard'])->one()->key;;
        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $adr);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
        $query = curl_exec($curl_handle);
        curl_close($curl_handle);
        $sum = json_decode($query);
        if($sum == null){
            $game['error'] = 'Summoner search error';
        } else{
            if ((isset($sum->status)) and ($sum->status->message == 'Forbidden')){
                $game['error'] = 'Riot keys error';
                die(json_encode($game));
            }
            if (isset($sum->id)){
                $adr = SiteController::RIOT_GAMES.'spectator/v3/active-games/by-summoner/'.$sum->id.'?api_key='.Secret::find()->where(['owner'=>'AnthonyWhitebeard'])->one()->key;;
                $curl_handle=curl_init();
                curl_setopt($curl_handle, CURLOPT_URL, $adr);
                curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
                $query = curl_exec($curl_handle);
                curl_close($curl_handle);
                $match = json_decode($query);
                $num = 0;
                $key = '?api_key='.Secret::find()->where(['owner'=>'AnthonyWhitebeard'])->one()->key;;
                if ($match !==null){
                    if ((isset($match->status->message)) and ($match->status->message == 'Data not found')){
                        $game['error'] = 'Match not found';
                    }
                    else{
                        foreach ($match->participants as $tard){
                            if ($key == 'api_key='.Secret::find()->where(['owner'=>'AnthonyWhitebeard'])->one()->key){
                                $key = 'api_key='.Secret::find()->where(['owner'=>'fantom79022'])->one()->key;
                            } else{
                                $key = 'api_key='.Secret::find()->where(['owner'=>'AnthonyWhitebeard'])->one()->key;;
                            }
                            $num++;
                            usleep(100000);
                            $adr = SiteController::RIOT_GAMES.'summoner/v3/summoners/'.$tard->summonerId.'?'.$key;
                            $curl_handle=curl_init();
                            curl_setopt($curl_handle, CURLOPT_URL, $adr);
                            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
                            $query = curl_exec($curl_handle);
                            curl_close($curl_handle);
                            $sum = json_decode($query);
                            if ($sum !== null){
                                if ((isset($sum->status)) and ($sum->status->message == 'Forbidden')){
                                    $game['error'] = 'Riot keys error';
                                    die(json_encode($game));
                                }
                                $team = $tard->teamId == 100 ? 1 : 2;
                                if (isset($sum->id)){
                                    $game['players']['team '.$team][$num]['summoner'] = $sum->name;
                                    $game['players']['team '.$team][$num]['champion'] = $tard->championId;
                                    $rt = Lolbase::find()->where(['id' => $sum->id])->one();
                                    if ($rt !== null){
                                        if ($rt->name !== null){
                                            $game['players']['team '.$team][$num]['name'] = $rt->name;
                                        }
                                        if ($rt->s_name !== null){
                                            $game['players']['team '.$team][$num]['s_name'] = $rt->s_name;
                                        }
                                        if ($rt->country !== null){
                                            $game['players']['team '.$team][$num]['country'] = $rt->country;
                                        }
                                        if ($rt->city !== null){
                                            $game['players']['team '.$team][$num]['city'] = $rt->city;
                                        }
                                        if ($rt->university !== null){
                                            $game['players']['team '.$team][$num]['university'] = $rt->university;
                                        }
                                        if ($rt->vk !== null){
                                            $game['players']['team '.$team][$num]['vk'] = $rt->vk;
                                        }
                                    }
                                }
                                else{
                                    $game['players'][$num]['error'] = 'not found';
                                }
                            }
                        }
                    }
                }
                else{
                    $game['error'] = 'Match search error';
                }

            }
            else{
                $game['error'] = 'Summoner not found';
            }
        }
        header('Content-Type: text/json;; charset=utf-8');
        die(json_encode($game, JSON_UNESCAPED_UNICODE));
    }

    //adding new phrases with api, updating, deleting
    public function actionAddphrase(){
        if ((isset($_GET['key'])) and (isset($_GET['answer']))){
            $set = Phrases::find()->where(['key' => $_GET['key']])->one();
            if ($set !== null){
                if (!isset($_GET['confirm'])) {
                    $result['error'] = 'Phrase with this key already set. If you still want to add it, repeat request with argument confirm=upd to update or confirm=del to delete';
                }
                else{
                    if ($_GET['confirm'] == 'add'){
                        $set = new Phrases();
                        $set->key = $_GET['key'];
                        $set->answer = $_GET['answer'];
                        $set->save();
                        $result['success'] = 'Sawed';
                        $result['key'] = $_GET['key'];
                        $result['answer'] = $_GET['answer'];
                    }
                    else{
                        if ($_GET['confirm'] == 'upd'){
                            $set->key = $_GET['key'];
                            $set->answer = $_GET['answer'];
                            $set->update();
                            $result['success'] = 'Updated';
                            $result['key'] = $_GET['key'];
                            $result['answer'] = $_GET['answer'];
                        }
                        else{
                            if ($_GET['confirm'] == 'del'){
                                $set->delete();
                                $result['success'] = 'deleted';
                                $result['key'] = $_GET['key'];
                            }
                            else{
                                $result['error'] = 'Unknown confirm argument';
                            }
                        }
                    }
                }
            }
            else{
                $set = new Phrases();
                $set->key = $_GET['key'];
                $set->answer = $_GET['answer'];
                $set->save();
                $result['success'] = 'Sawed';
                $result['key'] = $_GET['key'];
                $result['answer'] = $_GET['answer'];
            }
        }
        else{
            if ((isset($_GET['confirm'])) and (isset($_GET['key'])) and ($_GET['confirm'] == 'del')){
            $set = Phrases::find()->where(['key' => $_GET['key']])->one();
            if ($set !== null){
                $set->delete();
                $result['success'] = 'Deleted';
                $result['key'] = $_GET['key'];
                }
                else{
                    $result['error'] = 'Key = '.$_GET['key'].' does not found';
                }
            } else{
                $result['error'] = 'Need at least 2 params: key and answer';
            }
        }
        header('Content-Type: text/json;; charset=utf-8');
        die(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    //adding new loss with api, deleting
    public function actionAddloss(){
        if (isset($_GET['link'])){
            $set = Losses::find()->where(['link' => $_GET['link']])->one();
            if ($set == null){
                $set = new Losses();
                $set->link = $_GET['link'];
                $set->save();
                $result['success'] = 'Added';
                $result['link'] = $_GET['link'];
            }
            else{
                if (isset($_GET['confirm'])){
                    if ($_GET['confirm'] == 'del'){
                        $set->delete();
                        $result['success'] = 'Deleted';
                        $result['link'] = $_GET['link'];
                    }
                    else{
                        $result['error'] = 'Unknown confirm argument';
                    }
                }
                else{
                    $result['error'] = 'Loss already set. Repeat request with argument confirm=del to delete';
                }
            }
        }
        else{
            $result['error'] = 'Need at least 1 params: link';
        }
        header('Content-Type: text/json;; charset=utf-8');
        die(json_encode($result, JSON_UNESCAPED_UNICODE));
    }


    //Adding and updating Riot, TG, VK, keys
    public function actionNewkey(){
        if (isset($_GET['owner'])){
            if (isset($_GET['key'])){
                $oldkey = Secret::find()->where(['owner' => $_GET['owner']])->one();
                if ($oldkey !== null){
                    $oldkey->key = $_GET['key'];
                    $oldkey->update();
                    $result['success'] = 'updated';
                }
                else{
                    $result['error'] = 'owner not found';
                }
            }
            else{
                $result['error'] = 'Need at least 2 params: key and owner';
            }
        }
        else{
            $result['error'] = 'Need at least 2 params: key and owner';
        }
        header('Content-Type: text/json;; charset=utf-8');
        die(json_encode($result, JSON_UNESCAPED_UNICODE));    }

//    public function actionTest123(){
//        $losbase = Losses::find()->all();
//        $num = rand(0, count($losbase)-1);
//        $los = $losbase[$num]->link;
//        file_get_contents('https://api.telegram.org/bot'.Secret::find()->where(['owner' => 'BOT_API_KEY'])->one()->key.'/sendPhoto?chat_id='.'311962894'.'&photo='.$los);
//    }
}
