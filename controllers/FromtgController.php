<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class FromtgController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }



    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionAsk(){

    }


    public function actionSend(){
        $request = Yii::$app->request;
        $post = $request->post();


//            $file = time();
//            //если файла нету... тогда
//            if (!file_exists($file)) {
//
//                $fp = fopen($file, "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту),мы создаем файл
//                fwrite($fp, json_encode($post));
//                fclose($fp);
//            }




//        $token = $bot_api_key  = siteController::BOT_API_KEY;
//        $tg = new \TelegramBot\Api\BotApi(siteController::BOT_API_KEY);
//        $bot_username = '@tg_vk_sender_bot';
//        mb_internal_encoding("UTF-8");
//        try {
//            // Create Telegram API object
//            $telegram = new \Longman\TelegramBot\Telegram(siteController::BOT_API_KEY, $bot_username);
//
//            // Handle telegram webhook request
//            $telegram->handle();
//
//            $updates = json_decode(file_get_contents('php://input') ,true);
//            if (isset($updates['callback_query'])){
//                $msg = json_decode(json_encode($updates));
//            }
//            else{
//                $msg = json_decode($telegram->getCustomInput());
//            }
//
//
//        } catch (\Longman\TelegramBot\Exception\TelegramException $e) {
//            // Silence is golden!
//            // log telegram errors
//            // echo $e->getMessage();
//        }

    }
    private function parsing(){


        $tg = new \TelegramBot\Api\BotApi(siteController::BOT_API_KEY);
        $updates = json_decode(file_get_contents('php://input') ,true);


        $file = time();
        //если файла нету... тогда
        if (!file_exists($file)) {

            $fp = fopen($file, "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту),мы создаем файл
            fwrite($fp, json_encode(json_encode($updates)));
            fclose($fp);
        }


        if (isset($updates['callback_query'])){
            $msg = json_decode(json_encode($updates));
        }
        $bot = new \TelegramBot\Api\Client(siteController::BOT_API_KEY);
        if (isset($msg->message)) {
            if(stripos($msg->message->text, '/connection') == '0'){
                $i = 1;
                $a = 2;
            }
        }
    }
}
