﻿<?php
namespace app\controllers;
use app\models\BattleStatus;
use app\models\Links;
use app\models\Lolbase;
use app\models\Losses;
use app\models\Phrases;
use app\models\RoyalBattle;
use app\models\RoyalReg;
use app\models\RoyalStats;
use app\models\Secret;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


class RoyalController extends Controller
{
    const HOOK = 'https://doesitreallyworks.com';
    const BOT_API_KEY = '528796117:AAH4EF_WPr47NTZfj9JaApxe2Lsw1uZRhPA';
    const ACCESS_TOKEN = '227fa7d660f8b0daf02a197fd7bc4863e9e333f095ca2de4de9dbb721b464eb94236e87562111355a1b3d';
    const API_ADDRESS = 'http://vk-api-proxy.xtrafrancyz.net/method';
    const RIOT_API = 'api_key=RGAPI-37ab0c3a-ac1d-4aa1-8dc9-021f4b06262d';
    const RIOT_API2 = 'api_key=RGAPI-5211656b-421a-443b-bafd-983bc4d96fde';
    const RIOT_GAMES = 'https://ru.api.riotgames.com/lol/';

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function actionRoyalstat()
    {
        if (isset($_GET['chat'])) {
            $chat = $_GET['chat'];
            $stats = RoyalStats::find()->all();
            $n = 0;
            $text = 'Победители 👑 *КОРОЛЕВСКОЙ БИТВЫ* 👑 :';
            foreach ($stats as $stat) {
                $st[$stat->wins][$n]['user_id'] = $stat->user_id;
                $st[$stat->wins][$n]['name'] = $stat->name;
                $st[$stat->wins][$n]['wins'] = $stat->wins;
                $n++;
            }
            sort($st);
            foreach ($st as $winners) {
                foreach ($winners as $winner) {
                    $text = $text . '
' . $winner['name'] . ' - ' . $winner['wins'];
                }
            }
            $token = $bot_api_key = Secret::find()->where(['owner' => 'BOT_API_KEY'])->one()->key;
            $tg = new \TelegramBot\Api\BotApi($token);
            $tg->sendMessage($chat, $text, 'Markdown');
        }
    }

    public function actionRoyal(){
        sleep(5);
        set_time_limit(9999999);
        if (isset($_GET['chat'])){
            $chat = $_GET['chat'];
            $token = $bot_api_key  = Secret::find()->where(['owner' => 'BOT_API_KEY'])->one()->key;
            $tg = new \TelegramBot\Api\BotApi($token);
            RoyalBattle::deleteAll();
            $players = RoyalReg::find()->all();
            foreach ($players as $player){
                $pl = new RoyalBattle();
                if ((isset($player->username))){
                    $uname = '['.$player->username.'](tg://user?id='.$player->user_id.')';
                    $pl->name = $uname;
                } else{
                    $pl->name = '['.$player->name.'](tg://user?id='.$player->user_id.')';
                }
                $pl->user_id = $player->user_id;
                $pl->hp = 100;
                $pl->dmg = 20;
                $pl->mark = 50;
                $pl->save();
            }
            $tg->sendMessage($chat, 'ДА НАЧНЕТСЯ');
            sleep(5);
            $tg->sendMessage($chat, '👑 <b>КОРОЛЕВСКАЯ  БИТВА</b> 👑', 'HTML');
            sleep(5);
            $players = RoyalBattle::find()->all();
            $text = '';
            foreach ($players as $player){
                $members[$player->id]['name'] = $player->name;
                $members[$player->id]['user_id'] = $player->user_id;
                $members[$player->id]['hp'] = $player->hp;
                $members[$player->id]['dmg'] = $player->dmg;
                $members[$player->id]['mark'] = $player->mark;
                $text = $text.$player->name.'
';
            }
//            $text = substr($text, 0, -4);
            $tg->sendMessage($chat, 'Участники выпрыгивают из самолета', 'HTML');
            $msId = $this->sendlist($token, $text, $chat);
            $msId = 0;
            sleep(4);

            while (count($members) > 1){
                sort($members);
                $evDice = rand(0, 51);
                switch ($evDice){
                    case 0:
                        $plDice = rand(0, count($members)-1);
                        $dmgDice = rand(35, 70);
                        $members[$plDice]['hp'] = $members[$plDice]['hp']-$dmgDice;
                        if ($members[$plDice]['hp'] > 1){
                            $tg->sendMessage($chat, ''.$members[$plDice]['name'].' падает с обрыва, но выживает', 'Markdown');
                        } else{
                            $tg->sendMessage($chat, ''.$members[$plDice]['name'].' насмерть падает с обрыва', 'Markdown');
                        }
                        break;
                    case 1:
                        if (count($members) > 2){
                            $plDice = rand(0, count($members)-1);
                            do{
                                $pl2Dice = rand(0, count($members)-1);
                                $pl3Dice = rand(0, count($members)-1);
                            } while(($plDice == $pl2Dice) or ($plDice == $pl3Dice) or ($pl3Dice == $pl2Dice));
                            $members[$plDice]['hp'] = $members[$plDice]['hp'] - $members[$pl2Dice]['dmg'] - $members[$pl3Dice]['dmg'];
                            $members[$pl2Dice]['hp'] = $members[$plDice]['hp'] - $members[$plDice]['dmg'] - $members[$pl3Dice]['dmg'];
                            $members[$pl3Dice]['hp'] = $members[$plDice]['hp'] - $members[$plDice]['dmg'] - $members[$pl2Dice]['dmg'];
                            $text = $members[$plDice]['name'].', '.$members[$pl2Dice]['name'].' и '.$members[$pl3Dice]['name'].
                            ' сталкиваются в тяжелой битве. Всем сильно достается';
                            if (($members[$plDice]['hp'] < 1) and ($members[$pl2Dice]['hp'] < 1) and ($members[$pl3Dice]['hp'] < 1)){
                                $text = $text.' и никто не выживает';
                            } else{
                                if ($members[$plDice]['hp'] < 1){
                                    $text = $text.'. '.$members[$plDice]['name'].' пытается сбежать, но погибает от смертельного оскоррбления';
                                } else{
                                    $text = $text.'. '.$members[$plDice]['name'].' успешно сбегает отхватив от обоих';
                                }
                                if ($members[$pl2Dice]['hp'] < 1){
                                    $text = $text.'. '.$members[$pl2Dice]['name'].' получает несовместимй с жизнью нож в спину';
                                } else{
                                    $text = $text.'. '.$members[$pl2Dice]['name'].' отступает с ножом в спине';
                                }
                                if ($members[$pl3Dice]['hp'] < 1){
                                    $text = $text.'. '.$members[$pl3Dice]['name'].' коварно хихикает. Но когда гореч предательства запершила в горле, по его спине некому было постучать. '
                                    .$members[$pl3Dice]['name'].' погибает как предатель';
                                } else{
                                    $text = $text.'. '.$members[$pl3Dice]['name'].' коварно хихикает и уходит по своим делам';
                                }
                            }
                            $tg->sendMessage($chat, $text, 'Markdown');
                        }
                        break;
                    case 2:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = 'В поле обменялись парой выстрелов '.$members[$plDice]['name'].' и '.$members[$pl2Dice]['name'];
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - $members[$pl2Dice]['dmg']*3;
                        $members[$pl2Dice]['hp'] = $members[$pl2Dice]['hp'] - $members[$plDice]['dmg']*3;
                        if ($members[$pl2Dice]['hp'] < 1){
                            $text = $text.'. '.$members[$pl2Dice]['name']. ' переводится в состояние недвижимости';
                        } else{
                            $text = $text.'. '.$members[$pl2Dice]['name']. ' гордо переживает все попадания';
                        }
                        if ($members[$plDice]['hp'] < 1){
                            $text = $text.'. '.$members[$plDice]['name']. ' эмпиречески понимает, что кричать не помогает. Опыт стоит многого. В данном случае - жизни';
                        } else{
                            $text = $text.'. '.$members[$plDice]['name']. ' эмпиречески понимает, что кричать не помогает. Опыт стоит многого. В данном случае - здоровья';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 3:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = 'Зайдя на старый склад, '.$members[$plDice]['name'].' встречает '.$members[$pl2Dice]['name'].
                        ' и не думая, стреляет. '.$members[$pl2Dice]['name'].' вспоминает о вампирских корнях и высасывает немного жизни из противника';
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - $members[$pl2Dice]['dmg']*2;
                        $members[$pl2Dice]['hp'] = $members[$pl2Dice]['hp'] - $members[$plDice]['dmg']*3 + $members[$pl2Dice]['dmg'];
                        if ($members[$pl2Dice]['hp'] < 1){
                            $text = $text.'. '.$members[$pl2Dice]['name']. ' не соглашается умирать от руки смертного и принимает смертельную порцию чеснока';
                        } else{
                            $text = $text.'. '.$members[$pl2Dice]['name']. ' без труда переживает атаки жалкого смертного';
                        }
                        if ($members[$plDice]['hp'] < 1){
                            $text = $text.'. '.$members[$plDice]['name']. ' не собирается становится вампирской подстилкой. Он выбирает смерть';
                        } else{
                            $text = $text.'. '.$members[$plDice]['name']. ' с криком "DAGA KOTOWARU" убегает со страшного склада';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 4:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = 'Забравшись на дерево, '.$members[$plDice]['name'].' бросает яблоко в мирно гуляющего '.$members[$pl2Dice]['name'];
                        $members[$pl2Dice]['hp'] = $members[$pl2Dice]['hp'] - rand(20, 30);
                        if ($members[$pl2Dice]['hp'] < 1){
                            $text = $text.'. '.$members[$pl2Dice]['name']. ' чувствует себя Ньютоном. Таким же мертвым';
                        } else{
                            $text = $text.'. '.$members[$pl2Dice]['name']. ' чувствует себя Эйнштейном. А стоило бы Ньютоном';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 5:
                        $plDice = rand(0, count($members)-1);
                        $dmgDice = rand(40, 60);
                        $members[$plDice]['hp'] = $members[$plDice]['hp']-$dmgDice;
                        if ($members[$plDice]['hp'] < 1){
                            $tg->sendMessage($chat, $members[$plDice]['name'].'  случайно умирает', 'Markdown');
                        } else{
                            $tg->sendMessage($chat, $members[$plDice]['name'].' чуть не умирает', 'Markdown');
                        }
                        break;
                    case 6:
                        $plDice = rand(0, count($members)-1);
                        $dmgDice = rand(5, 10);
                        $members[$plDice]['hp'] = $members[$plDice]['hp']-$dmgDice;
                        $members[$plDice]['dmg'] = $members[$plDice]['dmg'] - 5;
                        if ($members[$plDice]['hp'] < 1){
                            $tg->sendMessage($chat, $members[$plDice]['name'].' насмерть ломает ноготь', 'Markdown');
                        } else{
                            $tg->sendMessage($chat, $members[$plDice]['name'].' ломает ноготь и терят в здоровье и уроне', 'Markdown');
                        }
                        break;
                    case 7:
                        $plDice = rand(0, count($members)-1);
                        $dmgDice = rand(30, 40);
                        $members[$plDice]['hp'] = $members[$plDice]['hp']-$dmgDice;
                        if ($members[$plDice]['hp'] < 1){
                            $tg->sendMessage($chat, $members[$plDice]['name'].' выпивает паленый Bacardi. И теряется', 'Markdown');
                        } else{
                            $tg->sendMessage($chat, 'Кажется, '.$members[$plDice]['name'].' выпивает паленый Bacardi. Утро будет тяжелым', 'Markdown');
                        }
                        break;
                    case 8:
                        $plDice = rand(0, count($members)-1);
                        $dmgDice = rand(40, 100);
                        $members[$plDice]['hp'] = $members[$plDice]['hp']-$dmgDice;
                        if ($members[$plDice]['hp'] < 1){
                            $tg->sendMessage($chat, 'Нельзя описать то, какой страшной смертью погибает '.$members[$plDice]['name'], 'Markdown');
                        } else{
                            $tg->sendMessage($chat, 'Нельзя описать то, что переживает '.$members[$plDice]['name'], 'Markdown');
                        }
                        break;
                    case 9:
                        $plDice = rand(0, count($members)-1);
                        $dmgDice = rand(30, 50);
                        $members[$plDice]['hp'] = $members[$plDice]['hp']-$dmgDice;
                        $text = $members[$plDice]['name'].' терпит нападение сначала злой собаки, а потом доброй. ';
                        if ($members[$plDice]['hp'] < 1){
                            $text = $text.$members[$plDice]['name'].' не переживает таких поворотов судьбы';
                        } else{
                            $text = $text.$members[$plDice]['name'].' радуется такой гармонии';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 10:
                        $plDice = rand(0, count($members)-1);
                        $dmgDice = rand(30, 50);
                        $members[$plDice]['hp'] = $members[$plDice]['hp']-$dmgDice;
                        $text = $members[$plDice]['name'].' старательно изображает смерть, что бы застать врагов врасплох. ';
                        if ($members[$plDice]['hp'] < 1){
                            $text = $text.'Так старательно, что верит себе';
                        } else{
                            $text = $text.'Так старательно, что получает много урона';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 11:
                        $plDice = rand(0, count($members)-1);
                        $dmgDice = rand(30, 50);
                        $members[$plDice]['hp'] = $members[$plDice]['hp']-$dmgDice;
                        if ($members[$plDice]['hp'] < 1){
                            $text = $members[$plDice]['name'].' умирает от переизбытка чувств. Чувства голода, холода и страха.';
                        } else{
                            $text = $members[$plDice]['name'].' едва не гибнет от переизбытка чувств. Преобладали чувства голода, холода и страха.';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 12:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['hp'] = 0;
                        if ($members[$plDice]['hp'] < 1){
                            $text = $members[$plDice]['name'].' не успевает за сужающейся зоной и умирает.';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 13:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['dmg'] = 30;
                        $tg->sendMessage($chat, $members[$plDice]['name'].' находит странной формы резиновую биту', 'Markdown');
                        break;
                    case 14:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['dmg'] = 5;
                        $tg->sendMessage($chat, $members[$plDice]['name'].' вооружается надувным топором. Урона конечно нет, зато миленько.', 'Markdown');
                        break;
                    case 15:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['dmg'] = 35;
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - 20;
                        if ($members[$plDice]['hp'] < 1){
                            $tg->sendMessage($chat, $members[$plDice]['name'].'  находит нунчаки и неумело жанглируя ими, отправляет себя в могилу', 'Markdown');
                        } else{
                            $tg->sendMessage($chat, $members[$plDice]['name'].' находит нунчаки. После пары синяков '.$members[$plDice]['name'].' осваивает новое оружие', 'Markdown');
                        }
                        break;
                    case 16:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['dmg'] = 30;
                        $tg->sendMessage($chat, 'В светящемся ящике '.$members[$plDice]['name'].' находит световой меч.', 'Markdown');
                        break;
                    case 17:
                        $plDice = rand(0, count($members)-1);
                        $evDice = rand(0, 1);
                        if ($evDice == 0 ){
                            $members[$plDice]['dmg'] = 35;
                            $tg->sendMessage($chat, 'Найдя дробовик, '.$members[$plDice]['name'].' решает сражатся в стиле Арнольда Шварцнегера', 'Markdown');
                        } else{
                            $members[$plDice]['hp'] = $members[$plDice]['hp'] - 300;
                            $tg->sendMessage($chat, 'Найдя дробовик, '.$members[$plDice]['name'].' решает сражатся в стиле Курта Кобейна', 'Markdown');
                        }
                        break;
                    case 18:
                        $plDice = rand(0, count($members)-1);
                        if ($members[$plDice]['hp'] < 100){
                            $members[$plDice]['hp'] = $members[$plDice]['hp'] + 40;
                            if ($members[$plDice]['hp'] > 100){
                                $members[$plDice]['hp'] = 100;
                            }
                            $tg->sendMessage($chat, $members[$plDice]['name'].' находит подорожник и прикладывает его к ранам', 'Markdown');
                        } else{
                            $tg->sendMessage($chat, $members[$plDice]['name'].' находит подорожник, но он ему ни к чему', 'Markdown');
                        }
                        break;
                    case 19:
                        $plDice = rand(0, count($members)-1);
                        if ($members[$plDice]['hp'] > 100){
                            $members[$plDice]['dmg'] = $members[$plDice]['dmg'] + 10;
                            $tg->sendMessage($chat, $members[$plDice]['name'].' находит зеленку и раскрашивает ею своё обмундирования, пугая врагов', 'Markdown');

                        }
                        else{
                            $members[$plDice]['hp'] = 100;
                            $tg->sendMessage($chat, $members[$plDice]['name'].' находит зеленку. Теперь '.$members[$plDice]['name'].
                                ' чувствует себя на все сто', 'Markdown');
                        }
                        break;
                    case 20:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] + 15;
                        $tg->sendMessage($chat, $members[$plDice]['name'].' открывает для себя, что под дождем можно есть суп вечно. После такой радости и здоровье растет', 'Markdown');
                        break;
                    case 21:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] + 25;
                        if ($members[$plDice]['hp'] > 100){
                            $members[$plDice]['hp'] = 100;
                        }
                        $tg->sendMessage($chat, $members[$plDice]['name'].' представляет Ситипаб. Здоровье поднимается', 'Markdown');
                        break;
                    case 22:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] + 40;
                        $members[$plDice]['dmg'] = $members[$plDice]['dmg'] + 10;
                        $tg->sendMessage($chat, $members[$plDice]['name'].' видит на дороге скатерть с едой и решает перекусить.'.
                    ' Хорошая еда и здоровье поправит, и сил добавит', 'Markdown');
                        break;
                    case 23:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] + 40;
                        $members[$plDice]['dmg'] = $members[$plDice]['dmg'] + 10;
                        $tg->sendMessage($chat, $members[$plDice]['name'].' находит в кустах настойку богатырышника и принимает внутрь', 'Markdown');
                        break;
                    case 24:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$plDice]['name'].' устраивает засаду, в которую попадается '.$members[$pl2Dice]['name'];
                        $members[$pl2Dice]['hp'] = $members[$pl2Dice]['hp'] - $members[$plDice]['dmg']*2;
                        $text = $text.'. '.$members[$plDice]['name'].' скрывается после двух попаданий, а';
                        if ($members[$pl2Dice]['hp'] < 1){
                            $text = $text.' '.$members[$pl2Dice]['name']. ' остается здесь навсегда';
                        } else{
                            $text = $text.' '.$members[$pl2Dice]['name']. ' приходит в себя, так ничего и не поняв';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 25:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$pl2Dice]['name'].' видит на дороге скатерть с едой и решает перекусить.';
                        $members[$pl2Dice]['hp'] = $members[$pl2Dice]['hp'] - 40;
                        $text = $text.'. '.'Зря, зря... Это '.$members[$plDice]['name'].' разбрасывает повсюду отравленую еду';
                        if ($members[$pl2Dice]['hp'] < 1){
                            $text = $text.' '.$members[$pl2Dice]['name'].' покидает состязание по причине внезапного приступа смерти';
                        } else{
                            $text = $text.' '.$members[$pl2Dice]['name'].' чувствует себя не очень хорошо';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 26:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$plDice]['name'].' пытается слиться с плоскостью параллельно оппоненту, но '.
                            $members[$pl2Dice]['name'].' переходит в неевклидово пространство и лупит противника.';
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - $members[$pl2Dice]['dmg'];
                        if ($members[$plDice]['hp'] < 1){
                            $text = $text.' '.$members[$plDice]['name'].' теперь не чувствует ног. Вообще больше ничего не чувствует';
                        } else{
                            $text = $text.' '.$members[$plDice]['name'].' с трудом возвращается к прежнему обьему, потеряв несколько очков здоровья';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 27:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$pl2Dice]['name'].' решает, что '.
                            $members[$plDice]['name'].' похож на старого врага и атакует три раза.';
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - $members[$pl2Dice]['dmg']*3;
                        if ($members[$plDice]['hp'] < 1){
                            $text = $text.' '.$members[$plDice]['name'].' отвечает на такое неистовство смертью';
                        } else{
                            $text = $text.' '.$members[$plDice]['name'].' героически спасается бегством';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 28:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$plDice]['name'].' кричит "ДЖЕРОНИМО" и бросается на противника. '.
                            $members[$pl2Dice]['name'].' отвечает, что это на французком и '.$members[$plDice]['name'].' решает отступить';
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 29:
                        $plDice = rand(0, count($members)-1);
                        $text = $members[$plDice]['name'].' жаждет крови и кусает свою руку.';
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - $members[$plDice]['dmg'];
                        if ($members[$plDice]['hp'] < 1){
                            $text = $text.' '.$members[$plDice]['name'].' не справляется с круговоротом крови в замкнутой системе и выбывает из турнира';
                        } else{
                            $text = $text.' '.$members[$plDice]['name'].' утоляет свою жажду ценой части здоровья';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 30:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$plDice]['name'].' замечает '.$members[$pl2Dice]['name'];
                        $members[$pl2Dice]['hp'] = $members[$pl2Dice]['hp'] - $members[$plDice]['dmg'];
                        $text = $text.'. '.$members[$plDice]['name'].' проводит одну точную атаку и ';
                        if ($members[$pl2Dice]['hp'] < 1){
                            $text = $text.' '.$members[$pl2Dice]['name']. ' умирает от потери здоровья';
                        } else{
                            $text = $text.' '.$members[$pl2Dice]['name']. ' решает, что рано ему на тот свет';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 31:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$plDice]['name'].' использует навык грозного взгляда';
                        $members[$pl2Dice]['hp'] = $members[$pl2Dice]['hp'] - $members[$plDice]['dmg']*2;
                        if ($members[$pl2Dice]['hp'] < 1){
                            $text = $text.'. '.$members[$pl2Dice]['name']. ' не может удержать сердце в груди. И проигрывает турнир';
                        } else{
                            $text = $text.'. '.$members[$pl2Dice]['name']. ' смутно понимает, чем заслужена такая немилость';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 32:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$pl2Dice]['name'].' увлекается своеобразной медитацией и не замечает внезапной атаки. '.$members[$plDice]['name'].
                        ' коварно потирает руки';
                        $members[$pl2Dice]['hp'] = $members[$pl2Dice]['hp'] - $members[$plDice]['dmg']*2;
                        if ($members[$pl2Dice]['hp'] < 1){
                            $text = $text.'. '.$members[$pl2Dice]['name']. ' не выдержав предательства, переезжает на кладбище';
                        } else{
                            $text = $text.'. '.$members[$pl2Dice]['name']. ' запоминает обидчика';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 33:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$pl2Dice]['name'].' находит потрёпанную тетрадь и аккуратно выводит в ней "'.$members[$plDice]['name'].
                        '"';
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - 70;
                        if ($members[$pl2Dice]['hp'] < 1){
                            $text = $text.'. '.$members[$plDice]['name']. ' умирает от остановки сердца';
                        } else{
                            $text = $text.'. '.$members[$plDice]['name']. ' чувствует злое присутствие';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 34:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$pl2Dice]['name'].' и '.$members[$plDice]['name'].' используют технику зеркального копирования и зеркально бьют друг друга лбами';
                        $members[$pl2Dice]['hp'] = $members[$pl2Dice]['hp'] - $members[$plDice]['dmg'];
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - $members[$pl2Dice]['dmg'];
                        if ($members[$pl2Dice]['hp'] < 1){
                            $text = $text.'. '.$members[$pl2Dice]['name']. ' посмертно получает осознания бесполезности этой техники';
                        } else{
                            $text = $text.'. '.$members[$pl2Dice]['name']. ' обижено потирает ушибленый лоб';
                        }
                        if ($members[$plDice]['hp'] < 1){
                            $text = $text.'. '.$members[$plDice]['name']. ' неправильно понимает суть техники и разбивается на осколки';
                        } else{
                            $members[$plDice]['hp'] = $members[$plDice]['hp'] - $members[$pl2Dice]['dmg'];
                            $members[$plDice]['hp'] = 100;
                            $text = $text.'. '.$members[$plDice]['name']. ' восстанавливает все здоровье после правильно выполненной техники';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 35:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['dmg'] = $members[$plDice]['dmg'] + 100;
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - rand(80, 110);
                        if ($members[$plDice]['hp'] < 1){
                            $tg->sendMessage($chat, 'В старом подвале '.$members[$plDice]['name'].'  видит старую книгу с кожаной обложкой. Открыв ее, '.$members[$plDice]['name'].
                                ' попадает в ад. Ты вообще фильмы смотришь, '.$members[$plDice]['name'].'?', 'Markdown');
                        } else{
                            $tg->sendMessage($chat, 'В старом подвале '.$members[$plDice]['name'].' видит старую книгу с кожаной обложкой. Открыв ее, '.$members[$plDice]['name'].
                                ' испытывает жуткую боль, но вместе с тем, получает чудовищную силу', 'Markdown');
                        }
                        break;
                    case 36:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['dmg'] = $members[$plDice]['dmg'] + 25;
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] + 50;
                            $tg->sendMessage($chat, ''.$members[$plDice]['name'].'  находит бронелифчик и немедленно одевает. '.$members[$plDice]['name']. ' на глазах у ошеломленных противников', 'Markdown');
                        break;
                    case 37:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['dmg'] = $members[$plDice]['dmg'] + 15;
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - 40;
                        if ($members[$plDice]['hp'] < 1){
                            $tg->sendMessage($chat, 'После укуса чумной крысы '.$members[$plDice]['name'].'  умерает с пеной во рту', 'Markdown');
                        } else{
                            $tg->sendMessage($chat, 'После укуса чумной крысы '.$members[$plDice]['name'].' получает бонус бешенства', 'Markdown');
                        }
                        break;
                    case 38:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - 20;
                        if ($members[$plDice]['hp'] < 1){
                            $tg->sendMessage($chat, 'Найдя хижину ведьмы, '.$members[$plDice]['name'].'  обучается логистике. Вот так сожаления могут привести к смерти', 'Markdown');
                        } else{
                            $tg->sendMessage($chat, 'Найдя хижину ведьмы, '.$members[$plDice]['name'].' обучается логистике. '.$members[$plDice]['name'].' негодует', 'Markdown');
                        }
                        break;
                    case 39:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] + 50;
                        $tg->sendMessage($chat, $members[$plDice]['name'].' открывает инвентарь и быстро съедает десять арбузов', 'Markdown');
                        break;
                    case 40:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] + 20;
                        $members[$plDice]['dmg'] = $members[$plDice]['dmg'] + 10;
                        $tg->sendMessage($chat, $members[$plDice]['name'].' немного подлетает, легко светясь. Левелап!', 'Markdown');
                        break;
                    case 41:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['dmg'] = 150;
                        $tg->sendMessage($chat, $members[$plDice]['name'].' находит BFG9000. ЧЕГО?', 'Markdown');
                        break;
                    case 42:
                        $plDice = rand(0, count($members)-1);
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - $members[$plDice]['dmg'];
                        if ($members[$plDice]['hp'] < 1){
                            $tg->sendMessage($chat, 'Увидев паука на ноге, '.$members[$plDice]['name'].' убивает его. Два кила одним ударом', 'Markdown');
                        } else{
                            $tg->sendMessage($chat, 'Увидев паука на ноге, '.$members[$plDice]['name'].' не оставляет ему шансов мощным ударом', 'Markdown');
                        }
                        break;
                    case 43:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$pl2Dice]['name'].' громко кричит: "'.$members[$plDice]['name'].
                            ', я тебя убиваю"';
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - 50;
                        if ($members[$plDice]['hp'] < 1){
                            $text = $text.'. '.$members[$plDice]['name']. ' уходит в мертвяк';
                        } else{
                            $text = $text.'. '.$members[$plDice]['name']. ' соглашается на компромис и теряет немного здоровья';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 44:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$pl2Dice]['name'].' предлагает поиграть в развивающие игры. ';
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - 40;
                        if ($members[$pl2Dice]['hp'] < 1){
                            $text = $text.' '.$members[$plDice]['name']. ' выбирает сыграть с ящик';
                        } else{
                            $text = $text.' '.$members[$plDice]['name']. ' тут же ловит дротик глазом';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 45:
                        $plDice = rand(0, count($members)-1);
                        $evDice = rand(0, 1);
                        if ($evDice == 0){
                            $members[$plDice]['hp'] = $members[$plDice]['hp'] + 60;
                            $tg->sendMessage($chat, 'Погнавшись за тремя зайцами, '.$members[$plDice]['name'].' ловит всех. И съедает', 'Markdown');
                        } else{
                            $members[$plDice]['hp'] = $members[$plDice]['hp'] - 40;
                            if ($members[$plDice]['hp'] < 1){
                                $tg->sendMessage($chat, 'Погнавшись за тремя зайцами, '.$members[$plDice]['name'].' насмерть падает в волчью яму', 'Markdown');
                            } else{
                                $tg->sendMessage($chat, 'Погнавшись за тремя зайцами, '.$members[$plDice]['name'].' падает в волчью яму', 'Markdown');
                            }
                        }
                        break;
                    case 46:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);

                        $text = $members[$pl2Dice]['name'].' напевает любимую песенку. '.$members[$plDice]['name'];
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] - 40;
                        if ($members[$plDice]['hp'] < 1){
                            $text = $text. ' по какому-то совпадению слышит вой баньши и умирает от разрыва сердца';
                        } else{
                            $text = $text.'. '.$members[$plDice]['name']. ' по какому-то совпадению слышит вой баньши';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 47:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$pl2Dice]['name'].' пытается перейти мост, но там тролит '.$members[$plDice]['name'];
                        if ($members[$pl2Dice]['dmg'] > 14){
                            $members[$pl2Dice]['dmg'] = $members[$pl2Dice]['dmg'] - 15;
                            $members[$plDice]['dmg'] = $members[$plDice]['dmg'] + 15;
                            $text = $text.'. '.$members[$pl2Dice]['name'].' жертвует часть своего обмундирования в фонд поддержки мостовых тролей';
                        } else{
                            $members[$pl2Dice]['hp'] = 0;
                            $text = $text.'. '.$members[$pl2Dice]['name'].' за неимением денег, жертвует все своё зроровье в фонд поддержки мостовых тролей';
                        }
                       $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 48:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = 'Луч света прорезал небо и нежно коснулся геройского лица. '.$members[$pl2Dice]['name'].' слепнет от такой красоты, что использует '.$members[$plDice]['name'];
                        $members[$pl2Dice]['hp'] = $members[$pl2Dice]['hp'] - $members[$plDice]['dmg']*2;
                        if ($members[$pl2Dice]['hp'] < 1){
                            $text = $text.' для нанесения двух смертельных ударов';
                        } else{
                            $text = $text.' для нанесения двух ударов';
                        }
                        $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 49:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$pl2Dice]['name'].' ничего не подозревая отдыхает в тени дерева, как вдруг  '
                            .$members[$plDice]['name'].' подкрадывается сзади и неожиданно обнимает противника. Все чувствуют себя лучше.';
                        $members[$plDice]['hp'] = $members[$plDice]['hp'] + 30;
                        $members[$pl2Dice]['hp'] = $members[$pl2Dice]['hp'] + 30;
                         $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    case 50:
                        $plDice = rand(0, count($members)-1);
                        do{
                            $pl2Dice = rand(0, count($members)-1);
                        } while($plDice == $pl2Dice);
                        $text = $members[$pl2Dice]['name'].' подкрадывается и крадет полоску здоровья противника, оставляя взамен свою. '
                            .$members[$plDice]['name'].' не против.';
                        $t = $members[$pl2Dice]['hp'];
                        $members[$pl2Dice]['hp'] = $members[$plDice]['hp'];
                        $members[$plDice]['hp'] = $t;
                        if ($members[$plDice]['hp'] > 100){
                            $members[$plDice]['hp'] = 100;
                        }
                        if ($members[$pl2Dice]['hp'] > 100){
                            $members[$pl2Dice]['hp'] = 100;
                        }
                         $tg->sendMessage($chat, $text, 'Markdown');
                        break;
                    default:
                        $plDice = rand(0, count($members)-1);
                        $dmgDice = rand(40, 70);
                        $members[$plDice]['hp'] = $members[$plDice]['hp']-$dmgDice;
                        if ($members[$plDice]['hp'] > 1){
                            $tg->sendMessage($chat, $members[$plDice]['name'].' наступает на мину и временно увеличивает свою скорость за счет здоровья', 'Markdown');
                        } else{
                            $tg->sendMessage($chat, $members[$plDice]['name'].' наступает на мину и улетает за пределы арены. Напоминаем, что арена ограничена рвом с кипящей лавой и аллигаторами', 'Markdown');
                        }
                }
                $n = 0;
                $resText = '';
                foreach ($members as $member){
                    if ($member['hp'] < 1){
                        unset($members[$n]);
                    }else{
                        $resText = $resText.$member['name'].'  --  *'.$member['hp'].' HP '. $member['dmg'].' DMG*
';
                    }
                $n++;
                }
                sort($members);
                if (isset($members[0])){
//                    $resText = substr($resText, 0, -2);
                    $msId = $this->sendlist($token, $resText, $chat, $msId);
                } else{
                    $msId = $this->sendlist($token, '💀💀💀💀💀', $chat, $msId);
                }
                sleep(rand(15, 20));
            }
            sort($members);
            if (isset($members[0])){
                $tg->sendMessage($chat, 'ОСТАЛСЯ ТОЛЬКО ОДИН', 'Markdown');
                $winner = RoyalStats::find()->where(['user_id' => $members[0]['user_id']])->limit(1)->one();
                if ($winner == null){
                    $winner = new RoyalStats();
                    $winner->user_id = $members[0]['user_id'];
                    $winner->name = $members[0]['name'];
                    $winner->wins = 1;
                    $winner->save();
                } else{
                    $winner->wins++;
                    $winner->update();
                }
                $tg->sendMessage($chat, 'В КОРОЛЕВСКОЙ БИТВЕ ПОБЕЖДАЕТ '.$members[0]['name'], 'Markdown');
            } else{
                $tg->sendMessage($chat, '👑 В КОРОЛЕВСКОЙ БИТВЕ 👑', 'HTML');
                $tg->sendMessage($chat, '*НИКТО НЕ ВЫЖИЛ*', 'Markdown');
            }
            RoyalReg::deleteAll();
            $status = BattleStatus::find()->where(['id' => 1])->one();
            $status->status = 'off';
            $status->update();
        }
    }
    public function sendlist($token, $text, $chat, $msgId = 0){
        $tg = new \TelegramBot\Api\BotApi($token);
        if ((isset($msgId)) and ($msgId !== 0)){
            $tg->deleteMessage($chat, $msgId);
        }
        $url = 'https://api.telegram.org/bot'.$token.'/sendMessage?chat_id='.
            $chat.'&text=Статус участников&parse_mode=Markdown';
        $options = array(
            'http'=>array(
                'header'=>'User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n'
            )
        );

        $context = stream_context_create($options);
        $newmsg = file_get_contents($url, false, $context);
        $newmsg = json_decode($newmsg);
        $newmsg = $newmsg->result->message_id;
//        echo $text;
//        echo '<br>';
//        echo $newmsg;
//        echo '<br>';
        $tg->editMessageText($chat, $newmsg, $text, 'Markdown');
        return $newmsg;
    }
}
