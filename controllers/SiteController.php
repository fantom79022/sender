<?php
namespace app\controllers;
use app\models\BattleStatus;
use app\models\ContactForm;
use app\models\Fantom;
use app\models\Links;
use app\models\Losses;
use app\models\Phrases;
use app\models\RoyalReg;
use app\models\Secret;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;


class SiteController extends Controller
{
    const HOOK = 'https://doesitreallyworks.com';
    const BOT_API_KEY = '528796117:AAH4EF_WPr47NTZfj9JaApxe2Lsw1uZRhPA';
    const ACCESS_TOKEN = '227fa7d660f8b0daf02a197fd7bc4863e9e333f095ca2de4de9dbb721b464eb94236e87562111355a1b3d';
    const API_ADDRESS = 'http://vk-api-proxy.xtrafrancyz.net/method';
    const RIOT_API = 'api_key=RGAPI-37ab0c3a-ac1d-4aa1-8dc9-021f4b06262d';
    const RIOT_API2 = 'api_key=RGAPI-5211656b-421a-443b-bafd-983bc4d96fde';
    const RIOT_GAMES = 'https://ru.api.riotgames.com/lol/';
    /**
     * @inheritdoc
     */

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAsdfg(){
        die('Привет');
    }

    private function addloss($tg, $msg){
        $msginfo = file_get_contents('https://api.telegram.org/bot'.SiteController::BOT_API_KEY.'/forwardMessage?message_id='.($msg->message->message_id-1).'&chat_id='.$msg->message->chat->id.'&from_chat_id='.$msg->message->from->id);
        $msginfo = json_decode($msginfo);
//        $file = 'qwe'.time();
//        //если файла нету... тогда
//        if (!file_exists($file)) {
//
//            $fp = fopen($file, "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту),мы создаем файл
//            fwrite($fp, json_encode($msginfo));
//            fclose($fp);
//        }
        if (isset($msginfo->result->photo)){
            foreach ($msginfo->result->photo as $phsize){
                $photolink = $phsize->file_id;
            }
            $file = file_get_contents('https://api.telegram.org/bot'.SiteController::BOT_API_KEY.'/getfile?file_id='.$photolink);
            $file = json_decode($file);
            $phname = 'loss'.time().'.jpg';
            if(copy('https://api.telegram.org/file/bot'.SiteController::BOT_API_KEY.'/'.$file->result->file_path, $phname)){
                $tg->sendMessage($msg->message->chat->id, 'Лосс сохранен');
                $newloss = new Losses();
                $newloss->link = 'http://doesitreallyworks.com/'.$phname;
                $newloss->save();
            }
            else{
                $tg->sendMessage($msg->message->chat->id, 'Лосс не сохранен');
            }
        }
        else{
            $tg->sendMessage($msg->message->chat->id, 'Лосс не найден');
        }
    }

    private function fantom(\TelegramBot\Api\BotApi $tg, $msg, $count = 1)
    {
        $set = [];
        for ($i = 1; $i <= $count; $i++) {
            $counter = new Fantom();
            $counter->count = 0;
            $counter->save();
            $set[] = [
                [
                    'text' => 'Дебил [0]',
                    'callback_data' => $counter->getPrimaryKey(),
                ],
            ];
        }
        $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
            $set
        );

        $tg->sendMessage($msg->message->chat->id, "Игра \"Фантом?\"", null, false, null, $keyboard);
    }


    public function debil($data)
    {
        $id = $data['callback_query']['data'];

        $counter = Fantom::findOne($id);
        $counter->count = $counter->count + 1;
        $counter->save();

        $token = Secret::find()->where(['owner' => 'BOT_API_KEY'])->one()->key;
        mb_internal_encoding("UTF-8");
        try {

            $set = $data['callback_query']['message']['reply_markup']['inline_keyboard'];
            foreach ($set as &$fantom) {
                if ($fantom['0']['callback_data'] == $id) {
                    $fantom['0']['text'] = 'Дебил ['.$counter->count.']';
                    break;
                }
            }
            $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                $set
            );
            $tg = new \TelegramBot\Api\BotApi($token);
            $tg->editMessageReplyMarkup(
                $data['callback_query']['message']['chat']['id'],
                $data['callback_query']['message']['message_id'],
                $keyboard
            );
        } catch (\Exception $e) {
        }
        die(200);
    }

    public function actionAsk()
    {


        try {
            $data = json_decode(file_get_contents('php://input'), true);


            if (false && isset($data['callback_query']['data'])) {
                $fact = $data;
                $fantom = $this;
                $fantom->debil($fact);
            }
        } catch (\Exception $e) {
        }


        $token = $bot_api_key  = Secret::find()->where(['owner' => 'BOT_API_KEY'])->one()->key;
        $bot_username = '@tg_vk_sender_bot';
        mb_internal_encoding("UTF-8");

        try {
            // Create Telegram API object
            $telegram = new \Longman\TelegramBot\Telegram($bot_api_key, $bot_username);

            // Handle telegram webhook request
            $telegram->handle();

            $updates = json_decode(file_get_contents('php://input') ,true);
            if (isset($updates['callback_query'])){
                $msg = json_decode(json_encode($updates));
            }
            else{
                $msg = json_decode($telegram->getCustomInput());
            }


        } catch (\Longman\TelegramBot\Exception\TelegramException $e) {
            // Silence is golden!
            // log telegram errors
            // echo $e->getMessage();
        }
        try {

            $tg = new \TelegramBot\Api\BotApi($token);

            if (isset($msg->message)){
//                $file = time();
//                //если файла нету... тогда
//                if (!file_exists($file)) {
//
//                    $fp = fopen($file, "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту),мы создаем файл
//                    fwrite($fp, json_encode($msg));
//                    fclose($fp);
//                }
                if (stripos($msg->message->text, 'connectioncode') == 1){
                    if ((stripos($msg->message->text, ' ')) !== false){
                        $code = substr($msg->message->text, (stripos($msg->message->text, ' '))+1);
                    }
                    else{
                        $code = substr($msg->message->text, 15);
                    }

                    $link = Links::find()->where(['special_code' => (string)($code)])->all();
                    if ($link == false){
                        $tg->sendMessage($msg->message->chat->id, 'Для начала подключитесь к чату ВКонтакте');
                    }
                    else{
                        foreach ($link as $lnk){
                            if ($lnk->tg_chat_id == null){
                                $lnk->tg_chat_id = (int)$msg->message->chat->id;
                                $lnk->update(false);
                            }
                            else{
                                if ($lnk->tg_chat_id !== (int)$msg->message->chat->id){
                                    $nlnk = new Links();
                                    $nlnk->vk_chat_id = $lnk->vk_chat_id;
                                    $nlnk->special_code = $lnk->special_code;
                                    $nlnk->tg_chat_id = (int)$msg->message->chat->id;
                                    $nlnk->conf = $lnk->conf;
                                    $nlnk->save(false);
                                }
                            }
                            $tg->sendMessage($msg->message->chat->id, 'Чаты успешно связаны');

                            return;
                        }
                    }
                }
                else{
                    $link = Links::find()->where(['tg_chat_id' => (int)$msg->message->chat->id])->all();
                    if (stripos($msg->message->text, 'setstatus') == 1) {
                        foreach ($link as $lnk) {

                            if ((stripos($msg->message->text, ' ')) !== false) {
                                $code = substr($msg->message->text, (stripos($msg->message->text, ' ')) + 1);
                            } else {
                                $code = substr($msg->message->text, 11);
                            }
                            $lnk->status = $code;
                            $lnk->update(false);
                        }
                    }
                    else{
                          if (((strripos(mb_strtolower($msg->message->text), 'заебал') !== FALSE) and (strripos(mb_strtolower($msg->message->text), 'лос') !== FALSE)) or
                            ((strripos(mb_strtolower($msg->message->text), 'нахуй') !== FALSE) and (strripos(mb_strtolower($msg->message->text), 'лос') !== FALSE))){
                            $tg->sendSticker($msg->message->chat->id, 'CAADBAADowUAAumyogxLsiAS9vfEfwI');
                        }
                        if (strripos(mb_strtolower($msg->message->text), 'бот, добавь лосс') !== FALSE){
                              $this->addloss($tg, $msg);
                        }
                        if (strripos(mb_strtolower($msg->message->text), 'бот, раздай фантома на одного') !== false) {
                            $this->fantom($tg, $msg);
                        }
                        if (strripos(mb_strtolower($msg->message->text), 'бот, раздай фантома на двоих') !== false) {
                            $this->fantom($tg, $msg, 2);
                        }
                        if (strripos(mb_strtolower($msg->message->text), 'бот, раздай фантома на троих') !== false) {
                            $this->fantom($tg, $msg, 3);
                        }
                        if (strripos(mb_strtolower($msg->message->text), 'бот, раздай фантома на четверых') !== false) {
                            $this->fantom($tg, $msg, 4);
                        }
                        if (strripos(mb_strtolower($msg->message->text), 'бот, раздай фантома на пятерых') !== false) {
                            $this->fantom($tg, $msg, 5);
                        }
                        if (strripos(mb_strtolower($msg->message->text), 'бот, раздай фантома на шестерых') !== false) {
                            $this->fantom($tg, $msg, 6);
                        }
                        if (strripos(mb_strtolower($msg->message->text), 'бот, раздай фантома на семерых') !== false) {
                            $this->fantom($tg, $msg, 7);
                        }
                        if (mb_strtolower($msg->message->text) === 'ясно') {
                            $this->clear($tg, $msg);
                        }
                        if ((strripos(mb_strtolower($msg->message->text), '!в самолет') !== FALSE) or (strripos(mb_strtolower($msg->message->text), '!в самолёт') !== FALSE)){
                            $status = BattleStatus::find()->where(['id' => 1])->limit(1)->one();
                            if ($status->status == 'reg'){
                                $user = RoyalReg::find()->where(['user_id' => $msg->message->from->id])->limit(1)->one();
                                if ($user !== null){
                                    $tg->sendMessage($msg->message->chat->id, 'Ты уже зарегестрирован', null, false, $msg->message->message_id);

                                    return;
                                } else {
                                    $user = new RoyalReg();
                                    if (isset($msg->message->from->username)){
                                        $user->username = $msg->message->from->username;
                                    }
                                    $user->user_id = $msg->message->from->id;
                                    $user->name = $msg->message->from->first_name;
                                    $user->save();
                                    $tg->sendMessage($msg->message->chat->id, 'Присаживайся поудобнее, скоро взлетаем', null, false, $msg->message->message_id);

                                    return;
                                }
                            } else{
                                $tg->sendMessage($msg->message->chat->id, 'Да ты поздно, попробуй в другой раз', null, false, $msg->message->message_id);

                                return;
                            }
                        }
                        if (strripos(mb_strtolower($msg->message->text), 'вылетаем!') !== FALSE){
                            if ((($msg->message->from->first_name == 'Anthony') and ($msg->message->from->last_name == 'Whitebeard'))
                            or ($msg->message->from->username == 'fantom79022')){
                                $status = BattleStatus::find()->where(['id' => 1])->limit(1)->one();
                                $status->status = 'battle';
                                $status->update();
                                $tg->sendMessage($msg->message->chat->id, 'Приготовьтесь ко взлету');
                                $url = 'http://doesitreallyworks.com/royal/royal?chat='.$msg->message->chat->id;
                                $ch = curl_init();
                                curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
                                curl_setopt( $ch, CURLOPT_URL, $url );
                                curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
                                curl_setopt( $ch, CURLOPT_ENCODING, "" );
                                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                                curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
                                curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );    # required for https urls
                                curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 1 );
                                curl_setopt( $ch, CURLOPT_TIMEOUT, 1 );
                                curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
                                $content = curl_exec( $ch );
                                $response = curl_getinfo( $ch );
                                curl_close ( $ch );

                                return;
                            } else {
                                $tg->sendMessage($msg->message->chat->id, 'У тебя здесь нет власти!', null, false, $msg->message->message_id);

                                return;
                            }
                        }
                        if (strripos(mb_strtolower($msg->message->text), 'слава победителям') !== FALSE){
                            get_headers('http://doesitreallyworks.com/royal/royalstat?chat='.$msg->message->chat->id);
                        }
                        if ((strripos(mb_strtolower($msg->message->text), 'что это такое? какой-то ивент?') !== FALSE) or
						(strripos(mb_strtolower($msg->message->text), '!битва') !== FALSE)){
                            if ((($msg->message->from->first_name == 'Anthony') and ($msg->message->from->last_name == 'Whitebeard'))
                                or ($msg->message->from->username == 'fantom79022')){
                                $status = BattleStatus::find()->where(['id' => 1])->limit(1)->one();
                                $status->status = 'reg';
                                $status->update();
                                $tg->sendMessage($msg->message->chat->id, 'Перед вами приземлился самолет!', null, false);
                                $tg->sendMessage($msg->message->chat->id, 'Если хотите зайти на борт, напишите "!в самолет"', null, false);

                                return;
                            } else {
                                $tg->sendMessage($msg->message->chat->id, 'У тебя здесь нет власти!', null, false, $msg->message->message_id);
                            }
                        }
                        if (strripos($msg->message->text, 'Кто это сделал') !== FALSE){

                            $tg->sendMessage($msg->message->chat->id, 'Это был я - Дио!', null, false, $msg->message->message_id);
                            $tg->sendSticker($msg->message->chat->id, 'CAADAgADPAADNCLqBT9ooyxFyhEGAg');
                        }
                        if ((mb_strtolower($msg->message->text) == 'loss pls') or (mb_strtolower($msg->message->text) == 'кинь лосс') or
                            (mb_strtolower($msg->message->text) == 'скинь лосс') or (mb_strtolower($msg->message->text) == 'cкинь мне лосс') or
                            (mb_strtolower($msg->message->text) == 'можно лосс?') or (mb_strtolower($msg->message->text) == 'киньте лосс')){
                            $losbase = Losses::find()->all();
                            $num = rand(0, count($losbase)-1);
                            $los = $losbase[$num]->link;
                            file_get_contents('https://api.telegram.org/bot'.Secret::find()->where(['owner' => 'BOT_API_KEY'])->one()->key.'/sendPhoto?chat_id='.$msg->message->chat->id.'&photo='.$los);

                            return;
                        }
                        $phrase = Phrases::find()->where(['key' => $msg->message->text])->one();
                        if ($phrase !== null){
                            $phrases = explode('|', $phrase->answer);
                            $count = count($phrases) - 1;
                            $message = $phrases[rand(0, $count)];
                            if ($message) {
                                $tg->sendMessage($msg->message->chat->id, $message, 'HTML', false, $msg->message->message_id);

                                return;
                            }
                        }
                        if ((strripos($msg->message->text, 'alive@vk-tg-senderbot') == 1) or (mb_strtolower($msg->message->text) == 'бот, ты жив?') or (mb_strtolower($msg->message->text) == 'бот, тебе норм?')){
                                $tg->sendMessage($msg->message->chat->id, 'Иди нахуй');

                                return;
                            }
                        if (($msg->message->chat->id == -1001148654922) or ($msg->message->chat->id == -318573766)){
                            if (strripos($msg->message->text, 'pidor@SublimeBot') == 1){
                                if ($msg->message->from->first_name == 'Tanteiv'){
                                    $num = rand(0, 6);
                                    switch ($num){
                                        case 0:
                                            $tg->sendMessage($msg->message->chat->id, 'Глеб поставил себе цель-
Открывает свой бордель.    
И стеснительно краснея,
Ищет он другого гея');
                                            break;
                                        case 1:
                                            $tg->sendMessage($msg->message->chat->id, 'Опять Глеб пидоров высматривает. Интересно, зачем?');
                                            break;
                                        case 2:
                                            $tg->sendMessage($msg->message->chat->id, 'Ни один пидор не уйдет сегодня от Глеба... просто так');
                                            break;
                                        case 3:
                                            $tg->sendMessage($msg->message->chat->id, 'Глеб пытается найти
Гейское лицо
Что бы вместе посливать
В ранкедах эльцо');
                                            break;
                                        case 4:
                                            $tg->sendMessage($msg->message->chat->id, 'Что б не тухло пламя-
брось в него углей
Глебу снова нужен
жаркий крепкий гей');
                                            break;
                                        case 5:
                                            $tg->sendMessage($msg->message->chat->id, 'Видимо, собрался Глеб
В даймонд, в долгий путь
Ищет он себе тиммейта
В лижку что б катнуть');
                                            break;
                                        default:
                                            $tg->sendMessage($msg->message->chat->id, 'Что бы мы делали без Глеба?');
                                    }
                                }
                                else{
                                    $num = rand(0, 3);
                                    switch ($num){
                                        case 0:
                                            $tg->sendMessage($msg->message->chat->id, 'Неужели не Глеб сбивает дейлик?');
                                            break;
                                        case 1:
                                            $tg->sendMessage($msg->message->chat->id, 'Так, стоп. Ты не Глеб! Зачем тебе это?');
                                            break;
                                        case 2:
                                            $tg->sendMessage($msg->message->chat->id, 'Глеб не сбил дейлик? Это же дейлик!');
                                            break;
                                        default:
                                            $tg->sendMessage($msg->message->chat->id, 'Так значит пидоров не только Глеб ищет?');
                                    }
                                }
                            }
                        }
                        foreach ($link as $lnk){
                            if (($lnk->status == 'all')OR($lnk->status == 'fromtg')OR($lnk->status == null)){
                                if (isset($msg->message->photo)){
                                    $ph = array_reverse($msg->message->photo);
                                    $photos = json_decode(file_get_contents('https://api.telegram.org/bot'.Secret::find()->where(['owner' => 'BOT_API_KEY'])->one()->key.'/getFile?file_id='.$ph[0]->file_id));
                                    $photo = '<br>https://api.telegram.org/file/bot'.Secret::find()->where(['owner' => 'BOT_API_KEY'])->one()->key.'/'.$photos->result->file_path;
                                }
                                else{
                                    $photo = '';
                                }

                                    $bodytag = str_replace('    
', '<br>', $msg->message->text);
                                $bodytag = str_replace(' ','%20',$bodytag);
                                if ($lnk->conf == '1'){
                                    file_get_contents(SiteController::API_ADDRESS.'/messages.send?access_token='.siteController::ACCESS_TOKEN.'&peer_id='.($lnk->vk_chat_id+2000000000).'&chat_id='.$lnk->vk_chat_id.'&title='.$msg->message->from->first_name.'+'.$msg->message->from->last_name.':&message='.$msg->message->from->first_name.'+'.$msg->message->from->last_name.':<br>'.$bodytag.$photo.'&v=5.73');
                                }
                                else{
                                    file_get_contents(SiteController::API_ADDRESS.'/messages.send?access_token='.siteController::ACCESS_TOKEN.'&peer_id='.($lnk->vk_chat_id).'&chat_id='.$lnk->vk_chat_id.'&title='.$msg->message->from->first_name.'+'.$msg->message->from->last_name.':&message='.$bodytag.$photo.'&v=5.73');
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (\Exception $e) {
            return 'not ok';
        }
        return 'ok';
    }

    public function actionSay()
    {
        $bot_api_key  = Secret::find()->where(['owner' => 'BOT_API_KEY'])->one()->key;
        $bot = new \TelegramBot\Api\Client($bot_api_key);
        if(!file_exists("registered.trigger")){
            /**
             * файл registered.trigger будет создаваться после регистрации бота.
             * если этого файла нет значит бот не зарегистрирован
             */

            // URl текущей страницы
            $hook = SiteController::HOOK;
            $result = $bot->setWebhook($hook.'/site/ask', '/etc/nginx/ssl/nginx.crt');
            if($result){
                file_put_contents("registered.trigger",time()); // создаем файл дабы прекратить повторные регистрации
            }
        }
        return $this->render('say');
    }
    public function actionRegistration()
    {
        return $this->render('registration');
    }

    public function actionLog()
    {
        return $this->render('log');
    }


    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */

    public function actionAbout()
    {
        return $this->render('about');
    }

    protected function clear($tg, $msg)
    {
        $answer = rand(1, 6);
        switch ($answer) {
            case 1:
                $tg->sendMessage($msg->message->chat->id, "Я тоже вижу прекрасный сон", null, false, $msg->message->message_id);
                break;
            case 2:
                $tg->sendMessage($msg->message->chat->id, "Понятно все", null, false, $msg->message->message_id);
                break;
            case 3:
                $tg->sendMessage($msg->message->chat->id, "Автору 0 лет", null, false, $msg->message->message_id);
                break;
            case 4:
                $tg->sendMessage($msg->message->chat->id, "Сасно", null, false, $msg->message->message_id);
                break;
            case 5:
                $tg->sendMessage($msg->message->chat->id, "Хуясно", null, false, $msg->message->message_id);
                break;
            default:
                break;
        }
    }
}
