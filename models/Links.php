<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "links".
 *
 * @property int $id
 * @property string $vk_chat_id
 * @property int $tg_chat_id
 * @property string $special_code
 * @property int $conf
 * @property string $status
 */
class Links extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'links';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vk_chat_id', 'special_code', 'status'], 'string'],
            [['tg_chat_id'], 'integer'],
            [['conf'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vk_chat_id' => 'Vk Chat ID',
            'tg_chat_id' => 'Tg Chat ID',
            'special_code' => 'Special Code',
            'conf' => 'Conf',
            'status' => 'Status',
        ];
    }
}
