<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "phrases".
 *
 * @property int $id
 * @property string $key
 * @property string $answer
 */
class Phrases extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'phrases';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'answer'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'answer' => 'Answer',
        ];
    }
}
