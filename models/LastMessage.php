<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "last_message".
 *
 * @property int $id
 * @property int $last_vk_message
 */
class LastMessage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'last_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['last_vk_message'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'last_vk_message' => 'Last Vk Message',
        ];
    }
}
