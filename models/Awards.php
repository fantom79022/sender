<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "awards".
 *
 * @property int $id
 * @property string $code
 * @property string $more
 */
class Awards extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'awards';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'more'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'more' => 'More',
        ];
    }
}
