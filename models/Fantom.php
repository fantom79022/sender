<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "awards".
 *
 * @property int $id
 * @property int $count
 */
class Fantom extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fantom';
    }
}
