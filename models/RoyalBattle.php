<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "royal_battle".
 *
 * @property int $id
 * @property string $name
 * @property int $hp
 * @property int $dmg
 * @property int $user_id
 * @property int $mark
 * @property string $username
 */
class RoyalBattle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'royal_battle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'username'], 'string'],
            [['hp', 'dmg', 'user_id', 'mark'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'hp' => 'Hp',
            'dmg' => 'Dmg',
            'user_id' => 'User ID',
            'mark' => 'Mark',
            'username' => 'Username',
        ];
    }
}
