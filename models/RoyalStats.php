<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "royal_stats".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property int $wins
 */
class RoyalStats extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'royal_stats';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'wins'], 'integer'],
            [['name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'wins' => 'Wins',
        ];
    }
}
