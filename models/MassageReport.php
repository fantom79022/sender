<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "massage_report".
 *
 * @property int $id
 * @property string $chat_id
 * @property string $massage_id
 * @property string $status
 */
class MassageReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'massage_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chat_id', 'massage_id', 'status'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chat_id' => 'Chat ID',
            'massage_id' => 'Massage ID',
            'status' => 'Status',
        ];
    }
}
