<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "royal_reg".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $username
 */
class RoyalReg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'royal_reg';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['name', 'username'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'username' => 'Username',
        ];
    }
}
