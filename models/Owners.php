<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "owners".
 *
 * @property int $id
 * @property string $owner
 * @property string $award_id
 * @property string $type
 */
class Owners extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'owners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['owner', 'award_id', 'type'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner' => 'Owner',
            'award_id' => 'Award ID',
            'type' => 'Type',
        ];
    }
}
