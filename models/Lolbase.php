<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lolbase".
 *
 * @property int $own_id
 * @property int $id
 * @property string $vk
 * @property string $university
 * @property string $summoner_name
 * @property string $country
 * @property string $city
 * @property string $name
 * @property string $s_name
 */
class Lolbase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lolbase';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['vk', 'university', 'summoner_name', 'country', 'city', 'name', 's_name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'own_id' => 'Own ID',
            'id' => 'ID',
            'vk' => 'Vk',
            'university' => 'University',
            'summoner_name' => 'Summoner Name',
            'country' => 'Country',
            'city' => 'City',
            'name' => 'Name',
            's_name' => 'S Name',
        ];
    }
}
