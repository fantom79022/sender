<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "secret".
 *
 * @property int $id
 * @property string $key
 * @property string $owner
 */
class Secret extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'secret';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'owner'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'owner' => 'Owner',
        ];
    }
}
